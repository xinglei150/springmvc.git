package com.demo.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.demo.service.LoginService;

@Controller
public class LoginController {
   
	@Resource
	private LoginService loginservice;
	
	@Resource
	private HttpServletRequest request;
	
	@RequestMapping("index")
	public ModelAndView index(){
		return new ModelAndView("login.jsp");
	}
	
	@RequestMapping("login")
	public ModelAndView doLogin(){
		String success="success.jsp";
		String fail="fail.jsp";
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		
		return loginservice.dologin(success,fail,username,password);
	}
}
